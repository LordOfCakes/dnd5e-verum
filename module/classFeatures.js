export const ClassFeatures = {
 "barbarian": {
   "subclasses": {
     "path-of-the-ancestral-guardian": {
       "label": "Path of the Ancestral Guardian",
       "source": "XGE pg. 9"
     },
     "path-of-the-battlerager": {
       "label": "Path of the Battlerager",
       "source": "SCAG pg. 121"
     },
     "path-of-the-berserker": {
       "label": "Path of the Berserker",
       "source": "PHB pg. 49",
       "features": {
         "3": ["Compendium.dndverum.classfeatures.CkbbAckeCtyHXEnL"],
         "6": ["Compendium.dndverum.classfeatures.0Jgf8fYY2ExwgQpN"],
         "10": ["Compendium.dndverum.classfeatures.M6VSMzVtKPhh8B0i"],
         "14": ["Compendium.dndverum.classfeatures.xzD9zlRP6dUxCtCl"]
       }
     },
     "path-of-the-juggernaut": {
       "label": "Path of the Juggernaut",
       "source": "TCS pg. 102"
     },
     "path-of-the-storm-herald": {
       "label": "Path of the Storm Herald",
       "source": "XGE pg. 10"
     },
     "path-of-the-totem-warrior": {
       "label": "Path of the Totem Warrior",
       "source": "PHB pg. 50; SCAG pg. 121"
     },
     "path-of-the-zealot": {
       "label": "Path of the Zealot",
       "source": "XGE pg. 11"
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.VoR0SUrNX5EJVPIO", "Compendium.dndverum.classfeatures.SZbsNbaxFFGwBpNK"],
     "2": ["Compendium.dndverum.classfeatures.SCVjqRdlZ9cvHVSR", "Compendium.dndverum.classfeatures.vt31lWAULygEl7yk"],
     "3": ["Compendium.dndverum.classfeatures.TH1QAf6YNGSeBVjT"],
     "5": ["Compendium.dndverum.classfeatures.XogoBnFWmCAHXppo", "Compendium.dndverum.classfeatures.Kl6zifJ5OmdHlOi2"],
     "7": ["Compendium.dndverum.classfeatures.NlXslw4yAqmKZWtN"],
     "9": ["Compendium.dndverum.classfeatures.L94gyvNpUhUe0rwh"],
     "11": ["Compendium.dndverum.classfeatures.FqfmbPgxiyrWzhYk"],
     "15": ["Compendium.dndverum.classfeatures.l8tUhZ5Pecm9wz7I"],
     "18": ["Compendium.dndverum.classfeatures.Q1exex5ALteprrPo"],
     "20": ["Compendium.dndverum.classfeatures.jVU4AgqfrFaqgXns"]
   }
 },
 "bard": {
   "subclasses": {
     "college-of-glamour": {
       "label": "College of Glamour",
       "source": "XGE pg. 14",
       "features": {}
     },
     "college-of-lore": {
       "label": "College of Lore",
       "source": "PHB pg. 54",
       "features": {
         "3": ["Compendium.dndverum.classfeatures.5zPmHPQUne7RDfaU"],
         "6": ["Compendium.dndverum.classfeatures.myBu3zi5eYvQIcuy"],
         "14": ["Compendium.dndverum.classfeatures.pquwueEMweRhiWaq"]
       }
     },
     "college-of-swords": {
       "label": "College of Swords",
       "source": "XGE pg. 15",
       "features": {}
     },
     "college-of-valor": {
       "label": "College of Valor",
       "source": "PHB pg. 55",
       "features": {}
     },
     "college-of-whispers": {
       "label": "College of Whispers",
       "source": "XGE pg. 16",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.hpLNiGq7y67d2EHA", "Compendium.dndverum.classfeatures.u4NLajXETJhJU31v"],
     "2": ["Compendium.dndverum.classfeatures.ezWijmCnlnQ9ZRX2", "Compendium.dndverum.classfeatures.he8RpPXwSl2lVSIk"],
     "3": ["Compendium.dndverum.classfeatures.ILhzFHiRrqgQ9dFJ", "Compendium.dndverum.classfeatures.aQLg7BWdRnm4Hr9S"],
     "5": ["Compendium.dndverum.classfeatures.3VDZGs5Ug3hIE322"],
     "6": ["Compendium.dndverum.classfeatures.SEJmsjkEhdAZ90ki"],
     "10": ["Compendium.dndverum.classfeatures.aonJ2YjkqkYB9WYB"],
     "20": ["Compendium.dndverum.classfeatures.GBYN5rH4nh1ocRlY"]
   }
 },
 "cleric": {
   "subclasses": {
     "ambition-domain": {
       "label": "Ambition Domain",
       "source": "PS:A pg. 27",
       "features": {}
     },
     "arcana-domain": {
       "label": "Arcana Domain",
       "source": "SCAG pg. 125",
       "features": {}
     },
     "blood-domain": {
       "label": "Blood Domain",
       "source": "TCS pg. 101",
       "features": {}
     },
     "death-domain": {
       "label": "Death Domain",
       "source": "DMG pg. 96",
       "features": {}
     },
     "forge-domain": {
       "label": "Forge Domain",
       "source": "XGE pg. 18",
       "features": {}
     },
     "grave-domain": {
       "label": "Grave Domain",
       "source": "XGE pg. 19",
       "features": {}
     },
     "knowledge-domain": {
       "label": "Knowledge Domain",
       "source": "PHB pg. 59",
       "features": {}
     },
     "life-domain": {
       "label": "Life Domain",
       "source": "PHB pg. 60",
       "features": {
         "1": ["Compendium.dndverum.classfeatures.68bYIOvx6rIqnlOW", "Compendium.dndverum.classfeatures.jF8AFfEMICIJnAkR", "Compendium.dndverum.spells.8dzaICjGy6mTUaUr", "Compendium.dndverum.spells.uUWb1wZgtMou0TVP"],
         "2": ["Compendium.dndverum.classfeatures.hEymt45rICi4f9eL"],
         "3": ["Compendium.dndverum.spells.F0GsG0SJzsIOacwV", "Compendium.dndverum.spells.JbxsYXxSOTZbf9I0"],
         "5": ["Compendium.dndverum.spells.ZU9d6woBdUP8pIPt", "Compendium.dndverum.spells.LmRHHMtplpxr9fX6"],
         "6": ["Compendium.dndverum.classfeatures.yv49QN6Bzqs0ecCs"],
         "7": ["Compendium.dndverum.spells.VtCXMdyM6mAdIJZb", "Compendium.dndverum.spells.TgHsuhNasPbhu8MO"],
         "8": ["Compendium.dndverum.classfeatures.T6u5z8ZTX6UftXqE"],
         "9": ["Compendium.dndverum.spells.Pyzmm8R7rVsNAPsd", "Compendium.dndverum.spells.AGFMPAmuzwWO6Dfz"],
         "17": ["Compendium.dndverum.classfeatures.4UOgxzr83vFuUash"]
       }
     },
     "light-domain": {
       "label": "Light Domain",
       "source": "PHB pg. 60",
       "features": {}
     },
     "nature-domain": {
       "label": "Nature Domain",
       "source": "PHB pg. 61",
       "features": {}
     },
     "order-domain": {
       "label": "Order Domain",
       "source": "GGR pg. 25",
       "features": {}
     },
     "solidarity-domain": {
       "label": "Solidarity Domain",
       "source": "PS:A pg. 24",
       "features": {}
     },
     "strength-domain": {
       "label": "Strength Domain",
       "source": "PS:A pg. 25",
       "features": {}
     },
     "tempest-domain": {
       "label": "Tempest Domain",
       "source": "PHB pg. 62",
       "features": {}
     },
     "trickery-domain": {
       "label": "Trickery Domain",
       "source": "PHB pg. 62",
       "features": {}
     },
     "war-domain": {
       "label": "War Domain",
       "source": "PHB pg. 63",
       "features": {}
     },
     "zeal-domain": {
       "label": "Zeal Domain",
       "source": "PS:A pg. 28",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.x637K2Icp2ZFM1TB", "Compendium.dndverum.classfeatures.v4gKwLhAq9vuqza7"],
     "2": ["Compendium.dndverum.classfeatures.YpiLQEKGalROn7iJ"],
     "5": ["Compendium.dndverum.classfeatures.NMy4piwXIpLjYbRE"],
     "10": ["Compendium.dndverum.classfeatures.eVXqHn0ojWrEuYGU"]
   },
 },
 "druid": {
   "subclasses": {
     "circle-of-dreams": {
       "label": "Circle of Dreams",
       "source": "XGE pg. 22",
       "features": {}
     },
     "circle-of-the-land": {
       "label": "Circle of the Land",
       "source": "PHB pg. 68",
       "features": {
         "2": ["Compendium.dndverum.classfeatures.lT8GsPOPgRzDC3QJ", "Compendium.dndverum.classfeatures.wKdRtFsvGfMKQHLY"],
         "3": ["Compendium.dndverum.classfeatures.YiK59gWSlcQ6Mbdz"],
         "6": ["Compendium.dndverum.classfeatures.3FB25qKxmkmxcxuC"],
         "10": ["Compendium.dndverum.classfeatures.OTvrJSJSUgAwXrWX"],
         "14": ["Compendium.dndverum.classfeatures.EuX1kJNIw1F68yus"]
       }
     },
     "circle-of-the-moon": {
       "label": "Circle of the Moon",
       "source": "PHB pg. 69",
       "features": {}
     },
     "circle-of-the-shepherd": {
       "label": "Circle of the Shepherd",
       "source": "XGE pg. 23",
       "features": {}
     },
     "circle-of-spores": {
       "label": "Circle of Spores",
       "source": "GGR pg. 26",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.LzJ5ayHt0OlSVGxi", "Compendium.dndverum.classfeatures.i6tPm3FNK13Ftc9v"],
     "2": ["Compendium.dndverum.classfeatures.swK0r5TOIxredxWS", "Compendium.dndverum.classfeatures.u6Du2P9s81SWuGbi"],
     "18": ["Compendium.dndverum.classfeatures.cVDEQo0ow1WJT7Wl", "Compendium.dndverum.classfeatures.xvgPu1O57DgXCM86"],
     "20": ["Compendium.dndverum.classfeatures.ip4bvmGoz3qkoqes"]
   },
 },
 "fighter": {
   "subclasses": {
     "arcane-archer": {
       "label": "Arcane Archer",
       "source": "XGE pg. 28",
       "features": {}
     },
     "banneret": {
       "label": "Banneret",
       "source": "SCAG pg. 128",
       "features": {}
     },
     "battle-master": {
       "label": "Battle Master",
       "source": "PHB pg. 73",
       "features": {}
     },
     "cavalier": {
       "label": "Cavalier",
       "source": "XGE pg. 30",
       "features": {}
     },
     "champion": {
       "label": "Champion",
       "source": "PHB pg. 72",
       "features": {
         "3": ["Compendium.dndverum.classfeatures.YgLQV1O849wE5TgM"],
         "7": ["Compendium.dndverum.classfeatures.dHu1yzIjD38BvGGd"],
         "11": ["Compendium.dndverum.classfeatures.kYJsED0rqqqUcgKz"],
         "15": ["Compendium.dndverum.classfeatures.aVKH6TLn1AG9hPSA"],
         "18": ["Compendium.dndverum.classfeatures.ipG5yx1tRNmeJfSH"]
       }
     },
     "echo-knight": {
       "label": "Echo Knight",
       "source": "EGW pg. 183",
       "features": {}
     },
     "eldritch-knight": {
       "label": "Eldritch Knight",
       "source": "PHB pg. 74",
       "features": {}
     },
     "samurai": {
       "label": "Samurai",
       "source": "XGE pg. 31",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.fbExzwNwEAl2kW9c", "Compendium.dndverum.classfeatures.nTjmWbyHweXuIqwc"],
     "2": ["Compendium.dndverum.classfeatures.xF1VTcJ3AdkbTsdQ"],
     "3": ["Compendium.dndverum.classfeatures.ax8M0X0q1GGWM26j"],
     "5": ["Compendium.dndverum.classfeatures.q9g1MLXuLZyxjQMg"],
     "9": ["Compendium.dndverum.classfeatures.653ZHbNcmm7ZGXbw"]
   },
 },
 "monk": {
   "subclasses": {
     "way-of-the-cobalt-soul": {
       "label": "Way of the Cobalt Soul",
       "source": "TCS pg. 104",
       "features": {}
     },
     "way-of-the-drunken-master": {
       "label": "Way of the Drunken Master",
       "source": "XGE pg. 33",
       "features": {}
     },
     "way-of-the-elements": {
       "label": "Way of the Four Elements",
       "source": "PHB pg. 80",
       "features": {}
     },
     "way-of-the-kensei": {
       "label": "Way of the Kensei",
       "source": "XGE pg. 34",
       "features": {}
     },
     "way-of-the-long-death": {
       "label": "Way of the Long Death",
       "source": "SCAG pg. 130",
       "features": {}
     },
     "way-of-the-open-hand": {
       "label": "Way of the Open Hand",
       "source": "PHB pg. 79",
       "features": {
         "3": ["Compendium.dndverum.classfeatures.iQxLNydNLlCHNKbp"],
         "6": ["Compendium.dndverum.classfeatures.Q7mOdk4b1lgjcptF"],
         "11": ["Compendium.dndverum.classfeatures.rBDZLatuoolT2FUW"],
         "17": ["Compendium.dndverum.classfeatures.h1gM8SH3BNRtFevE"]
       }
     },
     "way-of-the-shadow": {
       "label": "Way of Shadow",
       "source": "PHB pg. 80",
       "features": {}
     },
     "way-of-the-sun-soul": {
       "label": "Way of the Sun Soul",
       "source": "XGE pg. 35; SCAG pg. 131",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.UAvV7N7T4zJhxdfI", "Compendium.dndverum.classfeatures.l50hjTxO2r0iecKw"],
     "2": ["Compendium.dndverum.classfeatures.10b6z2W1txNkrGP7", "Compendium.dndverum.classfeatures.7vSrGc0MP5Vzm9Ac"],
     "3": ["Compendium.dndverum.classfeatures.rtpQdX77dYWbDIOH", "Compendium.dndverum.classfeatures.mzweVbnsJPQiVkAe"],
     "4": ["Compendium.dndverum.classfeatures.KQz9bqxVkXjDl8gK"],
     "5": ["Compendium.dndverum.classfeatures.XogoBnFWmCAHXppo", "Compendium.dndverum.classfeatures.pvRc6GAu1ok6zihC"],
     "6": ["Compendium.dndverum.classfeatures.7flZKruSSu6dHg6D"],
     "7": ["Compendium.dndverum.classfeatures.a4P4DNMmH8CqSNkC", "Compendium.dndverum.classfeatures.ZmC31XKS4YNENnoc"],
     "10": ["Compendium.dndverum.classfeatures.bqWA7t9pDELbNRkp"],
     "13": ["Compendium.dndverum.classfeatures.XjuGBeB8Y0C3A5D4"],
     "14": ["Compendium.dndverum.classfeatures.7D2EkLdISwShEDlN"],
     "15": ["Compendium.dndverum.classfeatures.gDH8PMrKvLHaNmEI"],
     "18": ["Compendium.dndverum.classfeatures.3jwFt3hSqDswBlOH"],
     "20": ["Compendium.dndverum.classfeatures.mQNPg89YIs7g5tG4"]
   },
 },
 "paladin": {
   "subclasses": {
     "oath-of-the-ancients": {
       "label": "Oath of the Ancients",
       "source": "PHB pg. 86",
       "features": {}
     },
     "oath-of-conquest": {
       "label": "Oath of Conquest",
       "source": "SCAG pg. 128",
       "features": {}
     },
     "oath-of-the-crown": {
       "label": "Oath of the Crown",
       "source": "SCAG pg. 132",
       "features": {}
     },
     "oath-of-devotion": {
       "label": "Oath of Devotion",
       "source": "PHB pg. 85",
       "features": {
         "3": ["Compendium.dndverum.spells.xmDBqZhRVrtLP8h2", "Compendium.dndverum.spells.gvdA9nPuWLck4tBl"],
         "5": ["Compendium.dndverum.spells.F0GsG0SJzsIOacwV", "Compendium.dndverum.spells.CylBa7jR8DSbo8Z3"],
         "9": ["Compendium.dndverum.spells.ZU9d6woBdUP8pIPt", "Compendium.dndverum.spells.15Fa6q1nH27XfbR8"],
         "13": ["Compendium.dndverum.spells.da0a1t2FqaTjRZGT", "Compendium.dndverum.spells.TgHsuhNasPbhu8MO"],
         "17": ["Compendium.dndverum.spells.d54VDyFulD9xxY7J", "Compendium.dndverum.spells.5e1xTohkzqFqbYH4"]
       }
     },
     "oathbreaker": {
       "label": "Oathbreaker",
       "source": "DMG pg. 97",
       "features": {}
     },
     "oath-of-redemption": {
       "label": "Oath of Redemption",
       "source": "XGE pg. 38",
       "features": {}
     },
     "oath-of-vengeance": {
       "label": "Oath of Vengeance",
       "source": "PHB pg. 87",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.E8ozg8avUVOX9N7u", "Compendium.dndverum.classfeatures.OdrvL3afwLOPeuYZ"],
     "2": ["Compendium.dndverum.classfeatures.ySMPQ6zNSlvkrl2f", "Compendium.dndverum.classfeatures.fbExzwNwEAl2kW9c", "Compendium.dndverum.classfeatures.ihoQHsmVZlyDbPhX"],
     "3": ["Compendium.dndverum.classfeatures.dY9yrqkyEDuF0CG2", "Compendium.dndverum.classfeatures.olAqNsUTIef9x8xC"],
     "5": ["Compendium.dndverum.classfeatures.XogoBnFWmCAHXppo"],
     "6": ["Compendium.dndverum.classfeatures.carGDhkIdoduTC0I"],
     "10": ["Compendium.dndverum.classfeatures.nahSkBO6LH4HkpaT"],
     "11": ["Compendium.dndverum.classfeatures.FAk41RPCTcvCk6KI"],
     "14": ["Compendium.dndverum.classfeatures.U7BIPVPsptBmwsnV"]
   },
 },
 "ranger": {
   "subclasses": {
     "beast-master": {
       "label": "Beast Master",
       "source": "PHB pg. 93",
       "features": {}
     },
     "gloom-stalker": {
       "label": "Gloom Stalker",
       "source": "XGE pg. 41",
       "features": {}
     },
     "horizon-walker": {
       "label": "Horizon Walker",
       "source": "XGE pg. 42",
       "features": {}
     },
     "hunter": {
       "label": "Hunter",
       "source": "PHB pg. 93",
       "features": {
         "3": ["Compendium.dndverum.classfeatures.wrxIW5sDfmGr3u5s"],
         "7": ["Compendium.dndverum.classfeatures.WgQrqjmeyMqDzVt3"],
         "11": ["Compendium.dndverum.classfeatures.7zlTRRXT1vWSBGjX"],
         "15": ["Compendium.dndverum.classfeatures.a0Sq88dgnREcIMfl"]
       }
     },
     "monster-slayer": {
       "label": "Monster Slayer",
       "source": "XGE pg. 43",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.4Vpj9vCOB37GtXk6", "Compendium.dndverum.classfeatures.8fbZt2Qh7ZttwIan"],
     "2": ["Compendium.dndverum.classfeatures.fbExzwNwEAl2kW9c", "Compendium.dndverum.classfeatures.u6xV3Ki3TXRrD7zg"],
     "3": ["Compendium.dndverum.classfeatures.1dJHU48yNqn3lcfx", "Compendium.dndverum.classfeatures.kaHcUGiwi8AtfZIm"],
     "5": ["Compendium.dndverum.classfeatures.XogoBnFWmCAHXppo"],
     "8": ["Compendium.dndverum.classfeatures.C5fzaOBc6HxyOWRn"],
     "10": ["Compendium.dndverum.classfeatures.r0unvWK0lPsDthDx"],
     "14": ["Compendium.dndverum.classfeatures.DhU2dWCNnX78TstR"],
     "18": ["Compendium.dndverum.classfeatures.QBVmY56RMQuh6C8h"],
     "20": ["Compendium.dndverum.classfeatures.3CaP1vFHVR8LgHjx"]
   },
 },
 "rogue": {
   "subclasses": {
     "arcane-trickster": {
       "label": "Arcane Trickster",
       "source": "PHB pg. 97",
       "features": {}
     },
     "assassin": {
       "label": "Assassin",
       "source": "PHB pg. 97",
       "features": {}
     },
     "inquisitive": {
       "label": "Inquisitive",
       "source": "XGE pg. 45",
       "features": {}
     },
     "mastermind": {
       "label": "Mastermind",
       "source": "XGE pg. 46; SCAG pg. 135",
       "features": {}
     },
     "scout": {
       "label": "Scout",
       "source": "XGE pg. 47",
       "features": {}
     },
     "swashbuckler": {
       "label": "Swashbuckler",
       "source": "XGE pg. 47; SCAG pg. 135",
       "features": {}
     },
     "thief": {
       "label": "Thief",
       "source": "PHB pg. 97",
       "features": {
         "3": ["Compendium.dndverum.classfeatures.ga3dt2zrCn2MHK8R", "Compendium.dndverum.classfeatures.FGrbXs6Ku5qxFK5G"],
         "9": ["Compendium.dndverum.classfeatures.Ei1Oh4UAA2E30jcD"],
         "13": ["Compendium.dndverum.classfeatures.NqWyHE7Rpw9lyKWu"],
         "17": ["Compendium.dndverum.classfeatures.LhRm1EeUMvp2EWhV"]
       }
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.3sYPftQKnbbVnHrh", "Compendium.dndverum.classfeatures.DPN2Gfk8yi1Z5wp7", "Compendium.dndverum.classfeatures.ohwfuwnvuoBWlSQr"],
     "2": ["Compendium.dndverum.classfeatures.01pcLg6PRu5zGrsb"],
     "3": ["Compendium.dndverum.classfeatures.80USV8ZFPIahpLd0"],
     "5": ["Compendium.dndverum.classfeatures.Mm64SKAHJWYecgXS"],
     "7": ["Compendium.dndverum.classfeatures.a4P4DNMmH8CqSNkC"],
     "11": ["Compendium.dndverum.classfeatures.YN9xm6MCvse4Y60u"],
     "14": ["Compendium.dndverum.classfeatures.fjsBk7zxoAbLf8ZI"],
     "15": ["Compendium.dndverum.classfeatures.V4pwFxlwHtNeB4w9"],
     "18": ["Compendium.dndverum.classfeatures.L7nJSRosos8sHJH9"],
     "20": ["Compendium.dndverum.classfeatures.rQhWDaMHMn7iU4f2"]
   },
 },
 "sorcerer": {
   "subclasses": {
     "draconic-bloodline": {
       "label": "Draconic Bloodline",
       "source": "PHB pg. 102",
       "features": {
         "1": ["Compendium.dndverum.classfeatures.EZsonMThTNLZq35j", "Compendium.dndverum.classfeatures.MW1ExvBLm8Hg82aA"],
         "6": ["Compendium.dndverum.classfeatures.x6eEZ9GUsuOcEa3G"],
         "14": ["Compendium.dndverum.classfeatures.3647zjKSE9zFwOXc"],
         "18": ["Compendium.dndverum.classfeatures.Gsha4bl0apxqspFy"]
       }
     },
     "divine-soul": {
       "label": "Divine Soul",
       "source": "XGE pg. 50",
       "features": {}
     },
     "pyromancer": {
       "label": "Pyromancer",
       "source": "PS:K pg. 9",
       "features": {}
     },
     "runechild": {
       "label": "Runechild",
       "source": "TCS pg. 103",
       "features": {}
     },
     "shadow-magic": {
       "label": "Shadow Magic",
       "source": "XGE pg. 50",
       "features": {}
     },
     "storm-sorcery": {
       "label": "Storm Sorcery",
       "source": "XGE pg. 51; SCAG pg. 137",
       "features": {}
     },
     "wild-magic": {
       "label": "Wild Magic",
       "source": "PHB pg. 103",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.cmRCL9T9UgRYOj1c", "Compendium.dndverum.classfeatures.oygRF3ZjTv2T7z0Y"],
     "2": ["Compendium.dndverum.classfeatures.LBKChJY5n02Afhnq"],
     "3": ["Compendium.dndverum.classfeatures.9Uh7uTDNZ04oTJsL"],
     "20": ["Compendium.dndverum.classfeatures.F2lEKSmOY0NUruzY"]
   },
 },
 "warlock": {
   "subclasses": {
     "the-archfey": {
       "label": "The Archfey",
       "source": "PHB pg. 108",
       "features": {}
     },
     "the-celestial": {
       "label": "The Celestial",
       "source": "XGE pg. 54",
       "features": {}
     },
     "the-fiend": {
       "label": "The Fiend",
       "source": "PHB pg. 109",
       "features": {
         "1": ["Compendium.dndverum.classfeatures.Jv0zu4BtUi8bFCqJ"],
         "6": ["Compendium.dndverum.classfeatures.OQSb0bO1yDI4aiMx"],
         "10": ["Compendium.dndverum.classfeatures.9UZ2WjUF2k58CQug"],
         "14": ["Compendium.dndverum.classfeatures.aCUmlnHlUPHS0rdu"]
       }
     },
     "the-hexblade": {
       "label": "The Hexblade",
       "source": "XGE pg. 55",
       "features": {}
     },
     "the-oldone": {
       "label": "The Great Old One",
       "source": "PHB pg. 109",
       "features": {}
     },
     "the-undying": {
       "label": "The Undying",
       "source": "SCAG pg. 139",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.jTXHaK0vvT5DV3uO", "Compendium.dndverum.classfeatures.x6IJZwr6f0SGral7"],
     "2": ["Compendium.dndverum.classfeatures.8MlxM2nEfE3Q0EVk"],
     "3": ["Compendium.dndverum.classfeatures.QwgfIpCN8VWfoUtX"],
     "11": ["Compendium.dndverum.classfeatures.zB77V8BcCJvWVxck"],
     "13": ["Compendium.dndverum.classfeatures.HBn6FXLz7Eiudz0V"],
     "15": ["Compendium.dndverum.classfeatures.knDZR4l4QfLTKinm"],
     "17": ["Compendium.dndverum.classfeatures.vMxJQEKeK6WwZFaF"],
     "20": ["Compendium.dndverum.classfeatures.0C04rwyvoknvFYiy"]
   },
 },
 "wizard": {
   "subclasses": {
     "school-of-abjuration": {
       "label": "School of Abjuration",
       "source": "PHB pg. 115",
       "features": {}
     },
     "school-of-bladesinging": {
       "label": "School of Bladesinging",
       "source": "SCAG pg. 141",
       "features": {}
     },
     "school-of-chronurgy-magic": {
       "label": "School of Chronurgy Magic",
       "source": "EGW pg. 185",
       "features": {}
     },
     "school-of-conjuration": {
       "label": "School of Conjuration",
       "source": "PHB pg. 116",
       "features": {}
     },
     "school-of-divination": {
       "label": "School of Divination",
       "source": "PHB pg. 116",
       "features": {}
     },
     "school-of-enchantment": {
       "label": "School of Enchantment",
       "source": "PHB pg. 117",
       "features": {}
     },
     "school-of-evocation": {
       "label": "School of Evocation",
       "source": "PHB pg. 117",
       "features": {
         "2": ["Compendium.dndverum.classfeatures.7uzJ2JkmsdRGLra3", "Compendium.dndverum.classfeatures.6VBXkjjBgjSpNElh"],
         "6": ["Compendium.dndverum.classfeatures.evEWCpE5MYgr5RRW"],
         "10": ["Compendium.dndverum.classfeatures.7O85kj6uDEG5NzUE"],
         "14": ["Compendium.dndverum.classfeatures.VUtSLeCzFubGXmGx"]
       }
     },
     "school-of-graviturgy-magic": {
       "label": "School of Graviturgy Magic",
       "source": "EGW pg. 185",
       "features": {}
     },
     "school-of-illusion": {
       "label": "School of Illusion",
       "source": "PHB pg. 118",
       "features": {}
     },
     "school-of-necromancy": {
       "label": "School of Necromancy",
       "source": "PHB pg. 118",
       "features": {}
     },
     "school-of-transmutation": {
       "label": "School of Transmutation",
       "source": "PHB pg. 119",
       "features": {}
     },
     "school-of-war-magic": {
       "label": "School of War Magic",
       "source": "XGE pg. 59",
       "features": {}
     }
   },
   "features": {
     "1": ["Compendium.dndverum.classfeatures.gbNo5eVPaqr8IVKL", "Compendium.dndverum.classfeatures.e0uTcFPpgxjIyUW9"],
     "2": ["Compendium.dndverum.classfeatures.AEWr9EMxy5gj4ZFT"],
     "18": ["Compendium.dndverum.classfeatures.JfFfHTeIszx1hNRx"],
     "20": ["Compendium.dndverum.classfeatures.nUrZDi6QN1YjwAr6", "Compendium.dndverum.classfeatures.31bKbWe9ZGVLEns6"]
   },
 }
};
