import {ClassFeatures} from "./classFeatures.js"

// Namespace Configuration Values
export const DND5E = {};

// ASCII Artwork
dndverum.ASCII = `_______________________________
______      ______ _____ _____
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__
| | | / _ \\/\\ | | |   \\ \\  __|
| |/ / (_>  < |/ //\\__/ / |___
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
dndverum.abilities = {
  "str": "dndverum.AbilityStr",
  "dex": "dndverum.AbilityDex",
  "con": "dndverum.AbilityCon",
  "int": "dndverum.AbilityInt",
  "wis": "dndverum.AbilityWis",
  "cha": "dndverum.AbilityCha"
};

dndverum.abilityAbbreviations = {
  "str": "dndverum.AbilityStrAbbr",
  "dex": "dndverum.AbilityDexAbbr",
  "con": "dndverum.AbilityConAbbr",
  "int": "dndverum.AbilityIntAbbr",
  "wis": "dndverum.AbilityWisAbbr",
  "cha": "dndverum.AbilityChaAbbr"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
dndverum.alignments = {
  'lg': "dndverum.AlignmentLG",
  'ng': "dndverum.AlignmentNG",
  'cg': "dndverum.AlignmentCG",
  'ln': "dndverum.AlignmentLN",
  'tn': "dndverum.AlignmentTN",
  'cn': "dndverum.AlignmentCN",
  'le': "dndverum.AlignmentLE",
  'ne': "dndverum.AlignmentNE",
  'ce': "dndverum.AlignmentCE"
};

/* -------------------------------------------- */

/**
 * An enumeration of item attunement types
 * @enum {number}
 */
dndverum.attunementTypes = {
  NONE: 0,
  REQUIRED: 1,
  ATTUNED: 2,
}

/**
 * An enumeration of item attunement states
 * @type {{"0": string, "1": string, "2": string}}
 */
dndverum.attunements = {
  0: "dndverum.AttunementNone",
  1: "dndverum.AttunementRequired",
  2: "dndverum.AttunementAttuned"
};

/* -------------------------------------------- */


dndverum.weaponProficiencies = {
  "sim": "dndverum.WeaponSimpleProficiency",
  "mar": "dndverum.WeaponMartialProficiency"
};

dndverum.toolProficiencies = {
  "art": "dndverum.ToolArtisans",
  "disg": "dndverum.ToolDisguiseKit",
  "forg": "dndverum.ToolForgeryKit",
  "game": "dndverum.ToolGamingSet",
  "herb": "dndverum.ToolHerbalismKit",
  "music": "dndverum.ToolMusicalInstrument",
  "navg": "dndverum.ToolNavigators",
  "pois": "dndverum.ToolPoisonersKit",
  "thief": "dndverum.ToolThieves",
  "vehicle": "dndverum.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
dndverum.timePeriods = {
  "inst": "dndverum.TimeInst",
  "turn": "dndverum.TimeTurn",
  "round": "dndverum.TimeRound",
  "minute": "dndverum.TimeMinute",
  "hour": "dndverum.TimeHour",
  "day": "dndverum.TimeDay",
  "month": "dndverum.TimeMonth",
  "year": "dndverum.TimeYear",
  "perm": "dndverum.TimePerm",
  "spec": "dndverum.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
dndverum.abilityActivationTypes = {
  "none": "dndverum.None",
  "action": "dndverum.Action",
  "bonus": "dndverum.BonusAction",
  "reaction": "dndverum.Reaction",
  "minute": dndverum.timePeriods.minute,
  "hour": dndverum.timePeriods.hour,
  "day": dndverum.timePeriods.day,
  "special": dndverum.timePeriods.spec,
  "legendary": "dndverum.LegAct",
  "lair": "dndverum.LairAct",
  "crew": "dndverum.VehicleCrewAction"
};

/* -------------------------------------------- */


dndverum.abilityConsumptionTypes = {
  "ammo": "dndverum.ConsumeAmmunition",
  "attribute": "dndverum.ConsumeAttribute",
  "material": "dndverum.ConsumeMaterial",
  "charges": "dndverum.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
dndverum.actorSizes = {
  "tiny": "dndverum.SizeTiny",
  "sm": "dndverum.SizeSmall",
  "med": "dndverum.SizeMedium",
  "lg": "dndverum.SizeLarge",
  "huge": "dndverum.SizeHuge",
  "grg": "dndverum.SizeGargantuan"
};

dndverum.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
dndverum.itemActionTypes = {
  "mwak": "dndverum.ActionMWAK",
  "rwak": "dndverum.ActionRWAK",
  "msak": "dndverum.ActionMSAK",
  "rsak": "dndverum.ActionRSAK",
  "save": "dndverum.ActionSave",
  "heal": "dndverum.ActionHeal",
  "abil": "dndverum.ActionAbil",
  "util": "dndverum.ActionUtil",
  "other": "dndverum.ActionOther"
};

/* -------------------------------------------- */

dndverum.itemCapacityTypes = {
  "items": "dndverum.ItemContainerCapacityItems",
  "weight": "dndverum.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
dndverum.limitedUsePeriods = {
  "sr": "dndverum.ShortRest",
  "lr": "dndverum.LongRest",
  "day": "dndverum.Day",
  "charges": "dndverum.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
dndverum.equipmentTypes = {
  "light": "dndverum.EquipmentLight",
  "medium": "dndverum.EquipmentMedium",
  "heavy": "dndverum.EquipmentHeavy",
  "bonus": "dndverum.EquipmentBonus",
  "natural": "dndverum.EquipmentNatural",
  "shield": "dndverum.EquipmentShield",
  "clothing": "dndverum.EquipmentClothing",
  "trinket": "dndverum.EquipmentTrinket",
  "vehicle": "dndverum.EquipmentVehicle"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
dndverum.armorProficiencies = {
  "lgt": dndverum.equipmentTypes.light,
  "med": dndverum.equipmentTypes.medium,
  "hvy": dndverum.equipmentTypes.heavy,
  "shl": "dndverum.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
dndverum.consumableTypes = {
  "ammo": "dndverum.ConsumableAmmunition",
  "potion": "dndverum.ConsumablePotion",
  "poison": "dndverum.ConsumablePoison",
  "food": "dndverum.ConsumableFood",
  "scroll": "dndverum.ConsumableScroll",
  "wand": "dndverum.ConsumableWand",
  "rod": "dndverum.ConsumableRod",
  "trinket": "dndverum.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
dndverum.currencies = {
  "pp": "dndverum.CurrencyPP",
  "gp": "dndverum.CurrencyGP",
  "ep": "dndverum.CurrencyEP",
  "sp": "dndverum.CurrencySP",
  "cp": "dndverum.CurrencyCP",
};


/**
 * Define the upwards-conversion rules for registered currency types
 * @type {{string, object}}
 */
dndverum.currencyConversion = {
  cp: {into: "sp", each: 10},
  sp: {into: "ep", each: 5 },
  ep: {into: "gp", each: 2 },
  gp: {into: "pp", each: 10}
};

/* -------------------------------------------- */


// Damage Types
dndverum.damageTypes = {
  "acid": "dndverum.DamageAcid",
  "bludgeoning": "dndverum.DamageBludgeoning",
  "cold": "dndverum.DamageCold",
  "fire": "dndverum.DamageFire",
  "force": "dndverum.DamageForce",
  "lightning": "dndverum.DamageLightning",
  "necrotic": "dndverum.DamageNecrotic",
  "piercing": "dndverum.DamagePiercing",
  "poison": "dndverum.DamagePoison",
  "psychic": "dndverum.DamagePsychic",
  "radiant": "dndverum.DamageRadiant",
  "slashing": "dndverum.DamageSlashing",
  "thunder": "dndverum.DamageThunder"
};

// Damage Resistance Types
dndverum.damageResistanceTypes = mergeObject(duplicate(dndverum.damageTypes), {
  "physical": "dndverum.DamagePhysical"
});


/* -------------------------------------------- */

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
dndverum.movementTypes = {
  "burrow": "dndverum.MovementBurrow",
  "climb": "dndverum.MovementClimb",
  "fly": "dndverum.MovementFly",
  "swim": "dndverum.MovementSwim",
  "walk": "dndverum.MovementWalk",
}

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @type {Object<string,string>}
 */
dndverum.movementUnits = {
  "ft": "dndverum.DistFt",
  "mi": "dndverum.DistMi"
}

/**
 * The valid units of measure for the range of an action or effect.
 * This object automatically includes the movement units from dndverum.movementUnits
 * @type {Object<string,string>}
 */
dndverum.distanceUnits = {
  "none": "dndverum.None",
  "self": "dndverum.DistSelf",
  "touch": "dndverum.DistTouch",
  "spec": "dndverum.Special",
  "any": "dndverum.DistAny"
};
for ( let [k, v] of Object.entries(dndverum.movementUnits) ) {
  dndverum.distanceUnits[k] = v;
}

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
dndverum.encumbrance = {
  currencyPerWeight: 50,
  strMultiplier: 15,
  vehicleWeightMultiplier: 2000 // 2000 lbs in a ton
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
dndverum.targetTypes = {
  "none": "dndverum.None",
  "self": "dndverum.TargetSelf",
  "creature": "dndverum.TargetCreature",
  "ally": "dndverum.TargetAlly",
  "enemy": "dndverum.TargetEnemy",
  "object": "dndverum.TargetObject",
  "space": "dndverum.TargetSpace",
  "radius": "dndverum.TargetRadius",
  "sphere": "dndverum.TargetSphere",
  "cylinder": "dndverum.TargetCylinder",
  "cone": "dndverum.TargetCone",
  "square": "dndverum.TargetSquare",
  "cube": "dndverum.TargetCube",
  "line": "dndverum.TargetLine",
  "wall": "dndverum.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are DND5E target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
dndverum.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
dndverum.healingTypes = {
  "healing": "dndverum.Healing",
  "temphp": "dndverum.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
dndverum.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * The set of possible sensory perception types which an Actor may have
 * @type {object}
 */
dndverum.senses = {
  "blindsight": "dndverum.SenseBlindsight",
  "darkvision": "dndverum.SenseDarkvision",
  "tremorsense": "dndverum.SenseTremorsense",
  "truesight": "dndverum.SenseTruesight"
};

/* -------------------------------------------- */

/**
 * The set of skill which can be trained
 * @type {Object}
 */
dndverum.skills = {
  "acr": "dndverum.SkillAcr",
  "ani": "dndverum.SkillAni",
  "arc": "dndverum.SkillArc",
  "ath": "dndverum.SkillAth",
  "dec": "dndverum.SkillDec",
  "his": "dndverum.SkillHis",
  "ins": "dndverum.SkillIns",
  "itm": "dndverum.SkillItm",
  "inv": "dndverum.SkillInv",
  "mar": "dndverum.SkillMar",
  "med": "dndverum.SkillMed",
  "nat": "dndverum.SkillNat",
  "prc": "dndverum.SkillPrc",
  "prf": "dndverum.SkillPrf",
  "per": "dndverum.SkillPer",
  "rel": "dndverum.SkillRel",
  "slt": "dndverum.SkillSlt",
  "ste": "dndverum.SkillSte",
  "sur": "dndverum.SkillSur"
};


/* -------------------------------------------- */

dndverum.spellPreparationModes = {
  "prepared": "dndverum.SpellPrepPrepared",
  "pact": "dndverum.PactMagic",
  "always": "dndverum.SpellPrepAlways",
  "atwill": "dndverum.SpellPrepAtWill",
  "innate": "dndverum.SpellPrepInnate"
};

dndverum.spellUpcastModes = ["always", "pact", "prepared"];

dndverum.spellProgression = {
  "none": "dndverum.SpellNone",
  "full": "dndverum.SpellProgFull",
  "half": "dndverum.SpellProgHalf",
  "third": "dndverum.SpellProgThird",
  "pact": "dndverum.SpellProgPact",
  "artificer": "dndverum.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
dndverum.spellScalingModes = {
  "none": "dndverum.SpellNone",
  "cantrip": "dndverum.SpellCantrip",
  "level": "dndverum.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
dndverum.weaponTypes = {
  "simpleM": "dndverum.WeaponSimpleM",
  "simpleR": "dndverum.WeaponSimpleR",
  "martialM": "dndverum.WeaponMartialM",
  "martialR": "dndverum.WeaponMartialR",
  "natural": "dndverum.WeaponNatural",
  "improv": "dndverum.WeaponImprov",
  "siege": "dndverum.WeaponSiege"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
dndverum.weaponProperties = {
  "ada": "dndverum.WeaponPropertiesAda",
  "amm": "dndverum.WeaponPropertiesAmm",
  "fin": "dndverum.WeaponPropertiesFin",
  "fir": "dndverum.WeaponPropertiesFir",
  "foc": "dndverum.WeaponPropertiesFoc",
  "hvy": "dndverum.WeaponPropertiesHvy",
  "lgt": "dndverum.WeaponPropertiesLgt",
  "lod": "dndverum.WeaponPropertiesLod",
  "mgc": "dndverum.WeaponPropertiesMgc",
  "rch": "dndverum.WeaponPropertiesRch",
  "rel": "dndverum.WeaponPropertiesRel",
  "ret": "dndverum.WeaponPropertiesRet",
  "sil": "dndverum.WeaponPropertiesSil",
  "spc": "dndverum.WeaponPropertiesSpc",
  "thr": "dndverum.WeaponPropertiesThr",
  "two": "dndverum.WeaponPropertiesTwo",
  "ver": "dndverum.WeaponPropertiesVer"
};


// Spell Components
dndverum.spellComponents = {
  "V": "dndverum.ComponentVerbal",
  "S": "dndverum.ComponentSomatic",
  "M": "dndverum.ComponentMaterial"
};

// Spell Schools
dndverum.spellSchools = {
  "abj": "dndverum.SchoolAbj",
  "con": "dndverum.SchoolCon",
  "div": "dndverum.SchoolDiv",
  "enc": "dndverum.SchoolEnc",
  "evo": "dndverum.SchoolEvo",
  "ill": "dndverum.SchoolIll",
  "nec": "dndverum.SchoolNec",
  "trs": "dndverum.SchoolTrs"
};

// Spell Levels
dndverum.spellLevels = {
  0: "dndverum.SpellLevel0",
  1: "dndverum.SpellLevel1",
  2: "dndverum.SpellLevel2",
  3: "dndverum.SpellLevel3",
  4: "dndverum.SpellLevel4",
  5: "dndverum.SpellLevel5",
  6: "dndverum.SpellLevel6",
  7: "dndverum.SpellLevel7",
  8: "dndverum.SpellLevel8",
  9: "dndverum.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
dndverum.spellScrollIds = {
  0: 'Compendium.dndverum.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.dndverum.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.dndverum.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.dndverum.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.dndverum.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.dndverum.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.dndverum.items.tI3rWx4bxefNCexS',
  7: 'Compendium.dndverum.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.dndverum.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.dndverum.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
dndverum.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];

/* -------------------------------------------- */

// Polymorph options.
dndverum.polymorphSettings = {
  keepPhysical: 'dndverum.PolymorphKeepPhysical',
  keepMental: 'dndverum.PolymorphKeepMental',
  keepSaves: 'dndverum.PolymorphKeepSaves',
  keepSkills: 'dndverum.PolymorphKeepSkills',
  mergeSaves: 'dndverum.PolymorphMergeSaves',
  mergeSkills: 'dndverum.PolymorphMergeSkills',
  keepClass: 'dndverum.PolymorphKeepClass',
  keepFeats: 'dndverum.PolymorphKeepFeats',
  keepSpells: 'dndverum.PolymorphKeepSpells',
  keepItems: 'dndverum.PolymorphKeepItems',
  keepBio: 'dndverum.PolymorphKeepBio',
  keepVision: 'dndverum.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
dndverum.proficiencyLevels = {
  0: "dndverum.NotProficient",
  1: "dndverum.Proficient",
  0.5: "dndverum.HalfProficient",
  2: "dndverum.Expertise"
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
dndverum.cover = {
  0: 'dndverum.None',
  .5: 'dndverum.CoverHalf',
  .75: 'dndverum.CoverThreeQuarters',
  1: 'dndverum.CoverTotal'
};

/* -------------------------------------------- */


// Condition Types
dndverum.conditionTypes = {
  "blinded": "dndverum.ConBlinded",
  "charmed": "dndverum.ConCharmed",
  "deafened": "dndverum.ConDeafened",
  "diseased": "dndverum.ConDiseased",
  "exhaustion": "dndverum.ConExhaustion",
  "frightened": "dndverum.ConFrightened",
  "grappled": "dndverum.ConGrappled",
  "incapacitated": "dndverum.ConIncapacitated",
  "invisible": "dndverum.ConInvisible",
  "paralyzed": "dndverum.ConParalyzed",
  "petrified": "dndverum.ConPetrified",
  "poisoned": "dndverum.ConPoisoned",
  "prone": "dndverum.ConProne",
  "restrained": "dndverum.ConRestrained",
  "stunned": "dndverum.ConStunned",
  "unconscious": "dndverum.ConUnconscious"
};

// Languages
dndverum.languages = {
  "common": "dndverum.LanguagesCommon",
  "aarakocra": "dndverum.LanguagesAarakocra",
  "abyssal": "dndverum.LanguagesAbyssal",
  "aquan": "dndverum.LanguagesAquan",
  "auran": "dndverum.LanguagesAuran",
  "celestial": "dndverum.LanguagesCelestial",
  "deep": "dndverum.LanguagesDeepSpeech",
  "draconic": "dndverum.LanguagesDraconic",
  "druidic": "dndverum.LanguagesDruidic",
  "dwarvish": "dndverum.LanguagesDwarvish",
  "elvish": "dndverum.LanguagesElvish",
  "giant": "dndverum.LanguagesGiant",
  "gith": "dndverum.LanguagesGith",
  "gnomish": "dndverum.LanguagesGnomish",
  "goblin": "dndverum.LanguagesGoblin",
  "gnoll": "dndverum.LanguagesGnoll",
  "halfling": "dndverum.LanguagesHalfling",
  "ignan": "dndverum.LanguagesIgnan",
  "infernal": "dndverum.LanguagesInfernal",
  "orc": "dndverum.LanguagesOrc",
  "primordial": "dndverum.LanguagesPrimordial",
  "sylvan": "dndverum.LanguagesSylvan",
  "terran": "dndverum.LanguagesTerran",
  "cant": "dndverum.LanguagesThievesCant",
  "undercommon": "dndverum.LanguagesUndercommon"
};

// Character Level XP Requirements
dndverum.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;

// Challenge Rating XP Levels
dndverum.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Character Features Per Class And Level
dndverum.classFeatures = ClassFeatures;

// Configure Optional Character Flags
dndverum.characterFlags = {
  "diamondSoul": {
    name: "dndverum.FlagsDiamondSoul",
    hint: "dndverum.FlagsDiamondSoulHint",
    section: "Feats",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "dndverum.FlagsElvenAccuracy",
    hint: "dndverum.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "dndverum.FlagsHalflingLucky",
    hint: "dndverum.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "dndverum.FlagsInitiativeAdv",
    hint: "dndverum.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "dndverum.FlagsAlert",
    hint: "dndverum.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "dndverum.FlagsJOAT",
    hint: "dndverum.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "dndverum.FlagsObservant",
    hint: "dndverum.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "powerfulBuild": {
    name: "dndverum.FlagsPowerfulBuild",
    hint: "dndverum.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "reliableTalent": {
    name: "dndverum.FlagsReliableTalent",
    hint: "dndverum.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "dndverum.FlagsRemarkableAthlete",
    hint: "dndverum.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "dndverum.FlagsWeaponCritThreshold",
    hint: "dndverum.FlagsWeaponCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "spellCriticalThreshold": {
    name: "dndverum.FlagsSpellCritThreshold",
    hint: "dndverum.FlagsSpellCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  },
  "meleeCriticalDamageDice": {
    name: "dndverum.FlagsMeleeCriticalDice",
    hint: "dndverum.FlagsMeleeCriticalDiceHint",
    section: "Feats",
    type: Number,
    placeholder: 0
  },
};

// Configure allowed status flags
dndverum.allowedActorFlags = ["isPolymorphed", "originalActor"].concat(Object.keys(dndverum.characterFlags));
